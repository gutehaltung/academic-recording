using FluentValidation;
using Models.ModelsDTO;

namespace WebAPI.Validations
{
    public class LecturerValidator : AbstractValidator<LecturerRecord>
    {
        public LecturerValidator()
        {
            RuleFor(lecturer => lecturer.Name).NotEmpty().Length(3, 50).WithErrorCode("ER020");
        }
    }
}