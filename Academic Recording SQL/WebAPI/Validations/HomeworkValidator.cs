using FluentValidation;
using Models.ModelsDTO;

namespace WebAPI.Validations
{
    public class HomeworkValidator : AbstractValidator<HomeworkRecord>
    {
        public HomeworkValidator()
        {
            RuleFor(homework => homework.Title).NotEmpty().Length(3, 50).WithErrorCode("ER010");
            RuleFor(homework => homework.Mark).InclusiveBetween(1, 5).WithErrorCode("ER011");
        }
    }
}