using FluentValidation;
using Models.Helpers;

namespace WebAPI.Validations
{
    public class StudentSearchCriteriaValidator : AbstractValidator<SearchCriteria.StudentSc>
    {
        public StudentSearchCriteriaValidator()
        {
            RuleFor(criteria => criteria.DisplayRecords.Skip)
                .GreaterThanOrEqualTo(0)
                .WithErrorCode("ER0030");
            
            RuleFor(criteria => criteria.DisplayRecords.Take)
                .InclusiveBetween(0, 1000)
                .WithErrorCode("ER0031");
            
            RuleFor(criteria => criteria.Name)
                .NotNull()
                .Length(1, 50)
                .WithErrorCode("ER0031");
            
            RuleFor(criteria => criteria.HomeworkMark.Min)
                .InclusiveBetween(1, 5)
                .WithErrorCode("ER0031");
            
            RuleFor(criteria => criteria.HomeworkMark.Max)
                .InclusiveBetween(1, 5)
                .WithErrorCode("ER0031");
        }
    }
}