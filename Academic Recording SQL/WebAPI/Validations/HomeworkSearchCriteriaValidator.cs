using FluentValidation;
using Models.Helpers;

namespace WebAPI.Validations
{
    public class HomeworkSearchCriteriaValidator : AbstractValidator<SearchCriteria.HomeworkSc>
    {
        public HomeworkSearchCriteriaValidator()
        {
            RuleFor(criteria => criteria.DisplayRecords.Skip)
                .GreaterThanOrEqualTo(0)
                .WithErrorCode("ER0040");

            RuleFor(criteria => criteria.DisplayRecords.Take)
                .InclusiveBetween(0, 1000)
                .WithErrorCode("ER0041");
            
            RuleFor(criteria => criteria.Title)
                .Length(3, 100)
                .WithErrorCode("ER0042");
            
            RuleFor(criteria => criteria.HomeworkMark.Min)
                .InclusiveBetween(0, 5)
                .WithErrorCode("ER0031");
            
            RuleFor(criteria => criteria.HomeworkMark.Max)
                .InclusiveBetween(0, 5)
                .WithErrorCode("ER0031");
        }
    }
}