using FluentValidation;
using Models.ModelsDTO;

namespace WebAPI.Validations
{
    public class StudentValidator : AbstractValidator<StudentRecord>
    {
        public StudentValidator()
        {
            RuleFor(student => student.Name).NotEmpty().Length(3, 50).WithErrorCode("ER000");
        }
    }
}