using Data.Data;
using Data.Services;
using Data.Services.Interfaces;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Models.Helpers;
using Models.Mapper;
using Models.ModelsDTO;
using WebAPI.Helpers;
using WebAPI.Validations;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(contextOptionsBuilder => contextOptionsBuilder
                .UseSqlServer(Configuration.GetConnectionString("MSSQLDefault"), builder => builder.MigrationsAssembly("WebAPI"))
            );

            services.AddControllers();

            services.AddMvc().AddFluentValidation();
            services.AddTransient<IValidator<StudentRecord>, StudentValidator>();
            services.AddTransient<IValidator<HomeworkRecord>, HomeworkValidator>();
            services.AddTransient<IValidator<LecturerRecord>, LecturerValidator>();
            services.AddTransient<IValidator<SearchCriteria.StudentSc>, StudentSearchCriteriaValidator>();
            services.AddTransient<IValidator<SearchCriteria.HomeworkSc>, HomeworkSearchCriteriaValidator>();
            services.AddTransient<IValidator<SearchCriteria.LecturerSc>, LecturerSearchCriteriaValidator>();

            services.AddAutoMapper(typeof(MapperProfile));

            services.AddScoped<IStudentServices, StudentServices>();
            services.AddScoped<IHomeworkServices, HomeworkServices>();
            services.AddScoped<ILecturerServices, LecturerServices>();

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "Academic Recording", Version = "v1"}); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Academic Recording"));
            }
            else
            {
                app.UseExceptionHandler(RouteNames.Error);
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}