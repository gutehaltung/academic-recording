using System.Threading.Tasks;
using Data.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.ModelsDTO;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route(RouteNames.Lecturer)]
    public class LecturerController : ControllerBase
    {
        private readonly ILecturerServices _lecturerServices;
        
        public LecturerController(ILecturerServices lecturerServices) => _lecturerServices = lecturerServices;

        [HttpGet("all/skip={skip:int}&take={take:int}/{name?}")]
        public async Task<IActionResult> GetLecturers(int skip, int take, string name)
        {
            var students = await _lecturerServices.GetLecturers(skip: skip, take: take, name: name);
            return students.Count == 0 ? NotFound() : Ok(students);
        }

        [HttpGet("single/id={id:int}")]
        public async Task<IActionResult> GetLecturerById(int id)
        {
            var student = await _lecturerServices.GetLecturerById(id);
            return student == null ? NotFound() : Ok(student);
        }

        [HttpPost]
        public async Task<IActionResult> AddLecturer(LecturerRecord lecturerToSave)
        {
            var newLecturer = await _lecturerServices.AddLecturer(lecturerToSave);
            return newLecturer == null ? NoContent() : Ok(newLecturer);
        }
        
        [HttpPut]
        public async Task<IActionResult> UpdateLecturer(LecturerRecord lecturerToUpdate)
        {
            var updatedLecturer = await _lecturerServices.UpdateLecturer(lecturerToUpdate);
            return updatedLecturer == null ? NoContent() : Ok(updatedLecturer);
        }
        
        [HttpDelete("delete/id={id:int}")]
        public async Task<IActionResult> DeleteLecturer(int id)
        {
            var isDeleted = await _lecturerServices.DeleteLecturer(id);
            return isDeleted ? Ok() : NoContent();
        }
    }
}