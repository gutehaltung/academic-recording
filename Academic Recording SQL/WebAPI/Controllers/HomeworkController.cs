using System.Threading.Tasks;
using Data.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.ModelsDTO;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route(RouteNames.Homework)]
    public class HomeworkController : ControllerBase
    {
        private readonly IHomeworkServices _homeworkServices;

        public HomeworkController(IHomeworkServices homeworkServices) => _homeworkServices = homeworkServices;

        [HttpGet("all/skip={skip:int}&take={take:int}/{markMin:int?}/{markMax:int?}/{title?}")]
        public async Task<IActionResult> GetHomeworks(int skip, int take, string title, int lecturerId,
            int studentId, bool? isMadeOnTime, int markMin = 1, int markMax = 5)
        {
            var homeworks = await _homeworkServices.GetHomeworks(skip: skip, take: take,
                markMin: markMin, markMax: markMax, title: title,
                isMadeOnTime: isMadeOnTime, lecturerId: lecturerId, studentId: studentId);
            return homeworks.Count > 0 ? Ok(homeworks) : NotFound();
        }

        [HttpGet("single/id={id:int}")]
        public async Task<IActionResult> GetHomeworkById(int id)
        {
            var homework = await _homeworkServices.GetHomeworkById(id);
            return homework == null ? NotFound() : Ok(homework);
        }

        [HttpPost]
        public async Task<IActionResult> AddHomework(HomeworkRecord homeworkToSave)
        {
            var newHomework = await _homeworkServices.AddHomework(homeworkToSave);
            return newHomework == null ? NoContent() : Ok(newHomework);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateHomework(HomeworkRecord homeworkToUpdate)
        {
            var updatedHomework = await _homeworkServices.UpdateHomework(homeworkToUpdate);
            return updatedHomework == null ? NoContent() : Ok(updatedHomework);
        }

        [HttpDelete("delete/id={id:int}")]
        public async Task<IActionResult> DeleteHomework(int id)
        {
            var isDeleted = await _homeworkServices.DeleteHomework(id);
            return isDeleted ? Ok() : NoContent();
        }
    }
}