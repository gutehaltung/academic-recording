using System;
using System.Data.Common;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        private readonly ILogger<ErrorController> _logger;

        //TODO: Add logger.
        public ErrorController(ILogger<ErrorController> logger) => _logger = logger;

        [Route(RouteNames.Error)]
        public IActionResult MainErrorResponse()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>().Error;
            switch (exception)
            {
                case ArgumentNullException:
                    _logger.LogError(exception, "An argument is null");
                    return BadRequest();
                case DbException:
                    _logger.LogError(exception, "Something wrong in a database");
                    return Problem();
                case DbUpdateException:
                    _logger.LogError(exception, "Database has got a problem during updating");
                    return Problem();
                case InvalidOperationException:
                    _logger.LogWarning(exception, "Error after attempt to find an element");
                    return Problem();
                default:
                    _logger.LogCritical(exception, "Unhandled exception");
                    return Problem();
            }
        }
    }
}