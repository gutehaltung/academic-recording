using System.Threading.Tasks;
using Data.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.ModelsDTO;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route(RouteNames.Student)]
    public class StudentController : ControllerBase
    {
        private readonly IStudentServices _studentServices;

        public StudentController(IStudentServices studentServices) => _studentServices = studentServices;

        [HttpGet("all/skip={skip:int}&take={take:int}/{markMin:int?}/{markMax:int?}/{name?}")]
        public async Task<IActionResult> GetStudents(int skip, int take, string name, int markMin = 1, int markMax = 5)
        {
            var students =
                await _studentServices.GetStudents(skip: skip, take: take, hwMin: markMin, hwMax: markMax, name: name);
            return students.Count > 0 ? Ok(students) : NotFound();
        }

        [HttpGet("single/id={id:int}")]
        public async Task<IActionResult> GetStudentById(int id)
        {
            var student = await _studentServices.GetStudentById(id);
            return student == null ? NotFound() : Ok(student);
        }

        [HttpPost]
        public async Task<IActionResult> AddStudent(StudentRecord studentToSave)
        {
            var student = await _studentServices.AddStudent(studentToSave);
            return student == null ? NoContent() : Ok(student);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateStudent(StudentRecord studentToUpdate)
        {
            var student = await _studentServices.UpdateStudent(studentToUpdate);
            return student == null ? NoContent() : Ok(student);
        }

        [HttpDelete("delete/id={id:int}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var isDelete = await _studentServices.DeleteStudent(id);
            return isDelete ? Ok() : NoContent();
        }
    }
}