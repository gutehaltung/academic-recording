namespace WebAPI.Helpers
{
    public static class RouteNames
    {
        public const string Error = "error";
        public const string Student = "Student";
        public const string Homework = "Homework";
        public const string Lecturer = "Lecturer";
    }
}