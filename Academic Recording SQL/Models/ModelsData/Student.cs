using System.Collections.Generic;

namespace Models.ModelsData
{
    public sealed class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Homework> Homeworks { get; set; } = new();
    }
}