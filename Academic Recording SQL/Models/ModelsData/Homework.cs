namespace Models.ModelsData
{
    public sealed class Homework
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Mark { get; set; }
        public bool IsMadeOnTime { get; set; } = true;
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int LecturerId { get; set; }
        public Lecturer Lecturer { get; set; }
    }
}