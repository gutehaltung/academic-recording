using System.Collections.Generic;

namespace Models.ModelsData
{
    public sealed class Lecturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Homework> Homeworks { get; set; } = new();
    }
}