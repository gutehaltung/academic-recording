using AutoMapper;
using Models.ModelsData;
using Models.ModelsDTO;

namespace Models.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Homework, HomeworkRecord>()
                .ForMember(homeworkRecord => homeworkRecord.Id, configurationExpression => configurationExpression.MapFrom(homework => homework.Id))
                .ForMember(homeworkRecord => homeworkRecord.Title, configurationExpression => configurationExpression.MapFrom(homework => homework.Title))
                .ForMember(homeworkRecord => homeworkRecord.Mark, configurationExpression => configurationExpression.MapFrom(homework => homework.Mark))
                .ForMember(homeworkRecord => homeworkRecord.IsMadeOnTime, configurationExpression => configurationExpression.MapFrom(homework => homework.IsMadeOnTime))
                .ForMember(homeworkRecord => homeworkRecord.StudentId, configurationExpression => configurationExpression.MapFrom(homework => homework.StudentId))
                .ForMember(homeworkRecord => homeworkRecord.Student, configurationExpression => configurationExpression.MapFrom(homework => homework.Student))
                .ForMember(homeworkRecord => homeworkRecord.LecturerId, configurationExpression => configurationExpression.MapFrom(homework => homework.LecturerId))
                .ForMember(homeworkRecord => homeworkRecord.Lecturer, configurationExpression => configurationExpression.MapFrom(homework => homework.Lecturer))
                .ReverseMap();
            CreateMap<Homework, HomeworkRecordBase>()
                .ForMember(homeworkRecord => homeworkRecord.Id, configurationExpression => configurationExpression.MapFrom(homework => homework.Id))
                .ForMember(homeworkRecord => homeworkRecord.Title, configurationExpression => configurationExpression.MapFrom(homework => homework.Title))
                .ForMember(homeworkRecord => homeworkRecord.Mark, configurationExpression => configurationExpression.MapFrom(homework => homework.Mark))
                .ForMember(homeworkRecord => homeworkRecord.IsMadeOnTime, configurationExpression => configurationExpression.MapFrom(homework => homework.IsMadeOnTime))
                .ReverseMap();
            
            CreateMap<Student, StudentRecord>()
                .ForMember(studentRecord => studentRecord.Id, configurationExpression => configurationExpression.MapFrom(student => student.Id))
                .ForMember(studentRecord => studentRecord.Name, configurationExpression => configurationExpression.MapFrom(student => student.Name))
                .ForMember(studentRecord => studentRecord.Homeworks, configurationExpression => configurationExpression.MapFrom(student => student.Homeworks))
                .ReverseMap();
            CreateMap<Student, StudentRecordBase>()
                .ForMember(studentRecord => studentRecord.Id, configurationExpression => configurationExpression.MapFrom(student => student.Id))
                .ForMember(studentRecord => studentRecord.Name, configurationExpression => configurationExpression.MapFrom(student => student.Name))
                .ReverseMap();
           
            CreateMap<Lecturer, LecturerRecord>()
                .ForMember(lecturerRecord => lecturerRecord.Id, configurationExpression => configurationExpression.MapFrom(lecturer => lecturer.Id))
                .ForMember(lecturerRecord => lecturerRecord.Name, configurationExpression => configurationExpression.MapFrom(lecturer => lecturer.Name))
                .ForMember(lecturerRecord => lecturerRecord.Homeworks, configurationExpression => configurationExpression.MapFrom(lecturer => lecturer.Homeworks))
                .ReverseMap();            
            CreateMap<Lecturer, LecturerRecordBase>()
                .ForMember(lecturerRecord => lecturerRecord.Id, configurationExpression => configurationExpression.MapFrom(lecturer => lecturer.Id))
                .ForMember(lecturerRecord => lecturerRecord.Name, configurationExpression => configurationExpression.MapFrom(lecturer => lecturer.Name))
                .ReverseMap();
        }
    }
}