namespace Models.Helpers
{
    public record SearchCriteria
    {
        public (int Skip, int Take) DisplayRecords { get; set; } = (0, 30);

        public sealed record StudentSc : SearchCriteria
        {
            public string Name { get; set; } = string.Empty;
            public (int Min, int Max) HomeworkMark { get; set; } = (0, 5);

            public StudentSc((int Skip, int Take) displayRecords, string name, (int Min, int Max) homeworkMark)
            {
                DisplayRecords = displayRecords;
                Name = name;
                HomeworkMark = homeworkMark;
            }
        }

        public sealed record HomeworkSc : SearchCriteria
        {
            public (int Min, int Max) HomeworkMark { get; set; } = (0, 5);
            public string Title { get; set; } = string.Empty;
            public bool? IsMadeOnTime { get; set; }
            public int? LecturerId { get; set; }
            public int? StudentId { get; set; }

            public HomeworkSc((int Skip, int Take) displayRecords, string title, (int Min, int Max) homeworkMark,
                bool? isMadeOnTime, int? lecturerId, int? studentId)
            {
                DisplayRecords = displayRecords;
                Title = title;
                HomeworkMark = homeworkMark;
                IsMadeOnTime = isMadeOnTime;
                LecturerId = lecturerId;
                StudentId = studentId;
            }
        }

        public sealed record LecturerSc : SearchCriteria
        {
            public string Name { get; set; } = string.Empty;

            public LecturerSc((int Skip, int Take) displayRecords, string name)
            {
                DisplayRecords = displayRecords;
                Name = name;
            }
        }
    }
}