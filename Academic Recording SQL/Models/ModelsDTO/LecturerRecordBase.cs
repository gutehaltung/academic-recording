namespace Models.ModelsDTO
{
    public record LecturerRecordBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}