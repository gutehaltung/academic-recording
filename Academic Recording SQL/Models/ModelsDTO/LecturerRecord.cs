using System.Collections.Generic;

namespace Models.ModelsDTO
{
    public record LecturerRecord : LecturerRecordBase
    {
        public List<HomeworkRecordBase> Homeworks { get; set; } = new();
    }
}