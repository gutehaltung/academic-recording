using System.Collections.Generic;

namespace Models.ModelsDTO
{
    public sealed record StudentRecord : StudentRecordBase
    {
        public List<HomeworkRecordBase> Homeworks { get; set; } = new();
    }
}