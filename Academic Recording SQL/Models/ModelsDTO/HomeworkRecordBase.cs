namespace Models.ModelsDTO
{
    public record HomeworkRecordBase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Mark { get; set; }
        public bool IsMadeOnTime { get; set; } = true;
    }
}