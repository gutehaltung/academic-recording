namespace Models.ModelsDTO
{
    public record StudentRecordBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}