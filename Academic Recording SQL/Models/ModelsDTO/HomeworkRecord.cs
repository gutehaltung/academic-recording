namespace Models.ModelsDTO
{
    public sealed record HomeworkRecord : HomeworkRecordBase
    {
        public int StudentId { get; set; }
        public StudentRecordBase Student { get; set; }
        public int LecturerId { get; set; }
        public LecturerRecordBase Lecturer { get; set; }
    }
}