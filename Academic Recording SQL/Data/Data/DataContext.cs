using System;
using Data.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Models.ModelsData;

namespace Data.Data
{
    public sealed class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> contextOptions) : base(contextOptions)
        {
        }

        public DbSet<Student> Students { set; get; }
        public DbSet<Homework> Homeworks { get; set; }
        public DbSet<Lecturer> Lecturers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder
            .LogTo(Console.WriteLine, new[] {RelationalEventId.CommandExecuted})
            .EnableSensitiveDataLogging();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasMany(student => student.Homeworks);
            modelBuilder.Entity<Lecturer>().HasMany(lecturer => lecturer.Homeworks);

            modelBuilder.Entity<Student>().HasData(DataGenerator.PersonGenerator());
            modelBuilder.Entity<Homework>().HasData(DataGenerator.HomeworkGenerator());
            modelBuilder.Entity<Lecturer>().HasData(DataGenerator.PersonGenerator());
        }
    }
}