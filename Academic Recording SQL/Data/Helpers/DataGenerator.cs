using System;
using System.Collections.Generic;

namespace Data.Helpers
{
    public static class DataGenerator
    {
        private const int Count = 5;
        private static readonly string[] Names = {"Tom", "Jim", "Sam", "Brandon", "Anna", "Jenifer", "Emma"};

        private static readonly string[] Subjects =
            {"Math", "Logic", "Chemistry", "Physics", "History", "Biology", "Language"};

        private static readonly Random Rnd = new();

        public static IEnumerable<object> PersonGenerator()
        {
            for (var i = 0; i < Count; i++)
                yield return new {Id = i + 1, Name = Names[Rnd.Next(Names.Length)]};
        }

        public static IEnumerable<object> HomeworkGenerator()
        {
            for (var i = 0; i < Count * 3; i++)
                yield return new
                {
                    Id = i + 1,
                    Title = Subjects[Rnd.Next(Subjects.Length)], 
                    IsMadeOnTime = Rnd.Next(1) > 0,
                    Mark = Rnd.Next(4) + 1,
                    StudentId = Rnd.Next(Count) + 1,
                    LecturerId = Rnd.Next(Count) + 1
                };
        }
    }
}