using System.Collections.Generic;
using System.Threading.Tasks;
using Models.ModelsDTO;

namespace Data.Services.Interfaces
{
    public interface IHomeworkServices
    {
        public Task<List<HomeworkRecord>> GetHomeworks(int skip, int take,
            string? title, int studentId, int lecturerId, bool? isMadeOnTime,
            int markMin = 0, int markMax = 5);
        public Task<HomeworkRecord?> GetHomeworkById(int id);
        public Task<HomeworkRecord?> AddHomework(HomeworkRecord homeworkToSave);
        public Task<HomeworkRecord?> UpdateHomework(HomeworkRecord homeworkToUpdate);
        public Task<bool> DeleteHomework(int id);
    }
}