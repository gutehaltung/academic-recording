using System.Collections.Generic;
using System.Threading.Tasks;
using Models.ModelsDTO;

namespace Data.Services.Interfaces
{
    public interface ILecturerServices
    {
        public Task<List<LecturerRecord>> GetLecturers(int skip, int take, string name = "");
        public Task<LecturerRecord?> GetLecturerById(int id);
        public Task<LecturerRecord?> AddLecturer(LecturerRecord lecturerToSave);
        public Task<LecturerRecord?> UpdateLecturer(LecturerRecord lecturerToUpdate);
        public Task<bool> DeleteLecturer(int id);
    }
}