using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Helpers;
using Models.ModelsDTO;

namespace Data.Services.Interfaces
{
    public interface IStudentServices
    {
        public Task<List<StudentRecord>> GetStudents(int skip, int take, string name, int hwMin, int hwMax);
        public Task<StudentRecord?> GetStudentById(int id);
        public Task<StudentRecord?> AddStudent(StudentRecord studentToSave);
        public Task<StudentRecord?> UpdateStudent(StudentRecord studentToUpdate);
        public Task<bool> DeleteStudent(int id);
    }
}