using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Data.Data;
using Data.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Helpers;
using Models.ModelsData;
using Models.ModelsDTO;

namespace Data.Services
{
    public class LecturerServices : ILecturerServices
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public LecturerServices(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<List<LecturerRecord>> GetLecturers(int skip, int take, string name = "")
        {
            var searchCriteria = new SearchCriteria.LecturerSc((Skip: skip, Take: take), name);
            var lecturers = await _dataContext.Lecturers
                .Skip(searchCriteria.DisplayRecords.Skip)
                .Take(searchCriteria.DisplayRecords.Take)
                .Include(lec => lec.Homeworks)
                .Where(lec => searchCriteria.Name == null || lec.Name.Contains(searchCriteria.Name))
                .ToListAsync();
            return _mapper.Map<List<LecturerRecord>>(lecturers);
        }

        public async Task<LecturerRecord?> GetLecturerById(int id)
        {
            var lecturer = await _dataContext.Lecturers
                .Include(lec => lec.Homeworks)
                .FirstOrDefaultAsync(lec => lec.Id == id);
            return lecturer == null ? null : _mapper.Map<LecturerRecord>(lecturer);
        }

        public async Task<LecturerRecord?> AddLecturer(LecturerRecord lecturerToSave)
        {
            var lecturer = _mapper.Map<Lecturer>(lecturerToSave);
            var lecturerEntityEntry = await _dataContext.Lecturers.AddAsync(lecturer);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<LecturerRecord>(lecturerEntityEntry.Entity) : null;
        }

        public async Task<LecturerRecord?> UpdateLecturer(LecturerRecord lecturerToUpdate)
        {
            var lecturer = _mapper.Map<Lecturer>(lecturerToUpdate);
            var lecturerEntityEntry = _dataContext.Lecturers.Update(lecturer);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<LecturerRecord>(lecturerEntityEntry.Entity) : null;
        }

        public async Task<bool> DeleteLecturer(int id)
        {
            var lecturer = await _dataContext.Lecturers.FirstOrDefaultAsync(lec => lec.Id == id);
            if (lecturer == null) return false;
            _dataContext.Lecturers.Remove(lecturer);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0;
        }
    }
}