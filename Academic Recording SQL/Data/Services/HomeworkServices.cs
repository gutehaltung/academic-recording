using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Data.Data;
using Data.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Helpers;
using Models.ModelsData;
using Models.ModelsDTO;

namespace Data.Services
{
    public class HomeworkServices : IHomeworkServices
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public HomeworkServices(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<List<HomeworkRecord>> GetHomeworks(int skip, int take,
            string? title, int studentId, int lecturerId, bool? isMadeOnTime,
            int markMin = 0, int markMax = 5)
        {
            var searchCriteria = new SearchCriteria
                .HomeworkSc(displayRecords: (Skip: skip, Take: take), homeworkMark: (Min: markMin, Max: markMax),
                    title: title, isMadeOnTime: isMadeOnTime, lecturerId: lecturerId, studentId: studentId);
            var homework = await _dataContext.Homeworks
                .Include(hw => hw.Lecturer)
                .Include(hw => hw.Student)
                .Where(hw => hw.Mark <= searchCriteria.HomeworkMark.Max && hw.Mark >= searchCriteria.HomeworkMark.Min)
                .Where(hw => !searchCriteria.IsMadeOnTime.HasValue || hw.IsMadeOnTime == searchCriteria.IsMadeOnTime) 
                .Where(hw => searchCriteria.LecturerId == 0 || hw.Lecturer.Id == searchCriteria.LecturerId)  
                .Where(hw => searchCriteria.StudentId == 0 || hw.Student.Id == searchCriteria.StudentId)  
                .ToListAsync();
            return _mapper.Map<List<HomeworkRecord>>(homework);
        }

        public async Task<HomeworkRecord?> GetHomeworkById(int id)
        {
            var homework = await _dataContext.Homeworks
                .Include(hw => hw.Lecturer)
                .Include(hw => hw.Student)
                .FirstOrDefaultAsync(hw => hw.Id == id);
            return homework == null ? null : _mapper.Map<HomeworkRecord>(homework);
        }

        public async Task<HomeworkRecord?> AddHomework(HomeworkRecord homeworkToSave)
        {
            var homework = _mapper.Map<Homework>(homeworkToSave);
            var homeworkEntryEntity = await _dataContext.Homeworks.AddAsync(homework);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<HomeworkRecord>(homeworkEntryEntity.Entity) : null;
        }

        public async Task<HomeworkRecord?> UpdateHomework(HomeworkRecord homeworkToUpdate)
        {
            var homework = _mapper.Map<Homework>(homeworkToUpdate);
            var homeworkEntryEntity = _dataContext.Homeworks.Update(homework);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<HomeworkRecord>(homeworkEntryEntity.Entity) : null;
        }

        public async Task<bool> DeleteHomework(int id)
        {
            var homework = await _dataContext.Homeworks.FirstOrDefaultAsync(hw => hw.Id == id);
            if (homework == null) return false;
            _dataContext.Homeworks.Remove(homework);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0;
        }
    }
}