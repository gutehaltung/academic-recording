using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Data.Data;
using Data.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Helpers;
using Models.ModelsData;
using Models.ModelsDTO;

namespace Data.Services
{
    public class StudentServices : IStudentServices
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public StudentServices(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<List<StudentRecord>> GetStudents(int skip, int take, string name, int hwMin, int hwMax)
        {
            var searchCriteria = new SearchCriteria.StudentSc((Skip: skip, Take: take), name, (Min: hwMin, Max: hwMax));
            var students = await _dataContext.Students
                .Skip(searchCriteria.DisplayRecords.Skip)
                .Take(searchCriteria.DisplayRecords.Take)
                .Include(student => student.Homeworks)
                .Where(student => searchCriteria.Name == null || student.Name.Contains(searchCriteria.Name))
                .Where(student => student.Homeworks.Average(homework => homework.Mark) >= searchCriteria.HomeworkMark.Min && student.Homeworks.Average(homework => homework.Mark) <= searchCriteria.HomeworkMark.Max)
                .ToListAsync();
            return _mapper.Map<List<StudentRecord>>(students);
        }

        public async Task<StudentRecord?> GetStudentById(int id)
        {
            var student = await _dataContext.Students
                .Include(st => st.Homeworks)
                .FirstOrDefaultAsync(st => st.Id == id);
            return student == null ? null : _mapper.Map<StudentRecord>(student);
        }

        public async Task<StudentRecord?> AddStudent(StudentRecord studentToSave)
        {
            var student = _mapper.Map<Student>(studentToSave);
            var studentEntityEntry = await _dataContext.Students.AddAsync(student);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<StudentRecord>(studentEntityEntry.Entity) : null;
        }

        public async Task<StudentRecord?> UpdateStudent(StudentRecord studentToUpdate)
        {
            var student = _mapper.Map<Student>(studentToUpdate);
            var studentEntityEntry = _dataContext.Students.Update(student);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0 ? _mapper.Map<StudentRecord>(studentEntityEntry.Entity) : null;
        }

        public async Task<bool> DeleteStudent(int id)
        {
            var student = await _dataContext.Students.FirstOrDefaultAsync(st => st.Id == id);
            if (student == null) return false;
            _dataContext.Students.Remove(student);
            var rows = await _dataContext.SaveChangesAsync();
            return rows > 0;
        }
    }
}